<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['namespace' => 'App\Http\Controllers'], function () {
	Route::group(['prefix' => 'v1', 'namespace' => 'Api'], function () {
		Route::group(['prefix' => 'user', 'namespace' => 'User'], function () {
			Route::get('/', 'UserController@index');
			Route::post('save', 'UserController@save');
			Route::get('show/{id}', 'UserController@show');
			Route::put('show/{id}', 'UserController@update');
			Route::delete('delete/{id}', 'UserController@delete');
		});
		Route::group(['prefix' => 'rol', 'namespace' => 'Rol'], function () {
			Route::get('/', 'RolController@index');
			Route::post('save', 'RolController@save');
			Route::get('show/{id}', 'RolController@show');
			Route::put('show/{id}', 'RolController@update');
			Route::delete('delete/{id}', 'RolController@delete');
		});
	});
});