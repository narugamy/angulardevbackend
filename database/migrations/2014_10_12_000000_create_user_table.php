<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTable extends Migration {

	public function up() {
		Schema::create('user', function (Blueprint $table) {
			$table->id();
			$table->string('name');
			$table->string('surname');
			$table->string('sex',1)->default('M');
			$table->date('date');
			$table->string('email', 191)->unique();
			$table->timestamp('email_verified_at')->nullable();
			$table->string('password');
			$table->rememberToken();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down() {
		Schema::dropIfExists('user');
	}
}
