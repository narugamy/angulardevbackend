<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable {

	use HasFactory, Notifiable, SoftDeletes;

	protected $table = 'user';

	protected $fillable = ['name', 'surname', 'sex', 'date', 'email', 'password',];

	protected $hidden = ['password', 'remember_token',];

	protected $casts = ['email_verified_at' => 'datetime',];

}
