<?php

namespace App\Models\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rol extends Model {

	use HasFactory, SoftDeletes;

	protected $table = 'rol';

	protected $fillable = ['name'];

}
