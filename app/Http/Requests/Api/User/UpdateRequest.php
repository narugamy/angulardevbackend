<?php

namespace App\Http\Requests\Api\User;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class UpdateRequest extends FormRequest {

	public function authorize() {
		return TRUE;
	}

	public function rules() {
		return [
			'name' => 'required|min:1|max:255',
			'surname' => 'required|min:1|max:255',
			'sex' => 'required|min:1|max:1',
			'date' => 'required|date',
			'email' => 'required|min:1|max:191|unique:user,email,'.$this->route()->parameter('id'),
		];
	}

	public function messages() {
		return [
			'name.required' => 'El nombre es requerido',
			'name.min' => 'La longitud minima del nombre es de 1',
			'name.max' => 'La longitud maxima del nombre es de 255',
			'surname.required' => 'El apellido es requerido',
			'surname.min' => 'La longitud minima del apellido es de 1',
			'surname.max' => 'La longitud maxima del apellido es de 255',
			'sex.required' => 'El sexo es requerido',
			'sex.min' => 'La longitud minima del sexo es de 1',
			'sex.max' => 'La longitud maxima del sexo es de 1',
			'date.required' => 'La fecha de nacimiento es requerida',
			'date.date' => 'El formado de fecha no es la correcta',
			'email.required' => 'El correo es requerido',
			'email.min' => 'La longitud minima del correo es de 1',
			'email.max' => 'La longitud maxima del correo es de 191',
			'email.unique' => 'El correo ya existe',
		];
	}

	protected function getValidatorInstance() {
		$data = array_replace_recursive($this->all(), $this->route()->parameters());
		$this->getInputSource()->replace($data);
		return parent::getValidatorInstance();
	}

	protected function failedValidation(Validator $validator) {
		$errors = (new ValidationException($validator))->errors();
		throw new HttpResponseException(response()->json(['errors' => $errors], JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
	}

}
