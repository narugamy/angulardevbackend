<?php

namespace App\Http\Requests\Api\Rol;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class UpdateRequest extends FormRequest {

	public function authorize() {
		return TRUE;
	}

	public function rules() {
		return [
			'name' => 'required|min:1|max:191|unique:rol,name,'.$this->route()->parameter('id'),
		];
	}

	public function messages() {
		return [
			'name.required' => 'El nombre es requerida',
			'name.unique' => 'El nombre ya existe',
			'name.min' => 'La longitud minima del nombre es de 1',
			'name.max' => 'La longitud maxima del nombre es de 191'
		];
	}

	protected function getValidatorInstance() {
		$data = array_replace_recursive($this->all(), $this->route()->parameters());
		$this->getInputSource()->replace($data);
		return parent::getValidatorInstance();
	}

	protected function failedValidation(Validator $validator) {
		$errors = (new ValidationException($validator))->errors();
		throw new HttpResponseException(response()->json(['errors' => $errors], JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
	}

}
