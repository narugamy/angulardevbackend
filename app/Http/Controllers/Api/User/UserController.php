<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\CreateRequest;
use App\Http\Requests\Api\User\UpdateRequest;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller {

	public function index(){
		return response()->json(['paginate' => User::withTrashed()->paginate(2)]);
	}

	public function save(CreateRequest $request){
		try {
			DB::beginTransaction();
			$user = new User($request->all());
			$user->fill(['date' => Carbon::parse($user->date)->toDateString(), 'password' => bcrypt('123')])->save();
			DB::commit();
			return response()->json(['message' => 'Registro exitoso']);
		}catch (Exception $exception){
			DB::rollBack();
			return response()->json(['message' => 'Ocurrio un error en el proceso', 'exception' => $exception->getMessage()], 400);
		}
	}

	public function show($id){
		$user = User::find($id);
		if($user):
			return response()->json(['user' => $user]);
		endif;
		return response()->json(['message' => 'El usuario no existe'], 400);
	}

	public function update(UpdateRequest $request, $id){
		$user = User::find($id);
		if($user):
			try {
				DB::beginTransaction();
				$params = $request->except('deleted_at');
				$params['date'] = Carbon::parse($params['date'], 'America/Lima')->toDateString();
				$user->fill($params)->save();
				DB::commit();
				return response()->json(['message' => 'Actualizacion exitosa']);
			}catch (Exception $exception){
				DB::rollBack();
				return response()->json(['message' => 'Ocurrio un error en el proceso', 'exception' => $exception->getMessage()], 400);
			}
		endif;
		return response()->json(['message' => 'El usuario no existe'], 400);
	}

	public function delete($id){
		$user = User::withTrashed()->find($id);
		if($user):
			try {
				DB::beginTransaction();
				if (empty($user->deleted_at)) :
					$user->delete();
					DB::commit();
					return response()->json(['message' => 'Desactivación exitosa']);
				endif;
				$user->restore();
				DB::commit();
				return response()->json(['message' => 'Activación exitosa']);
			}catch (Exception $exception){
				DB::rollBack();
				return response()->json(['message' => 'Ocurrio un error en el proceso', 'exception' => $exception->getMessage()], 400);
			}
		endif;
		return response()->json(['message' => 'El usuario no existe'], 400);
	}

}
