<?php

namespace App\Http\Controllers\Api\Rol;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Rol\CreateRequest;
use App\Http\Requests\Api\Rol\UpdateRequest;
use App\Models\Model\Rol;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RolController extends Controller {

	public function index(){
		return response()->json(['paginate' => Rol::withTrashed()->paginate(2)]);
	}

	public function save(CreateRequest $request){
		try {
			DB::beginTransaction();
			$rol = new Rol($request->all());
			$rol->save();
			DB::commit();
			return response()->json(['message' => 'Registro exitoso']);
		}catch (Exception $exception){
			DB::rollBack();
			return response()->json(['message' => 'Ocurrio un error en el proceso', 'exception' => $exception->getMessage()], 400);
		}
	}

	public function show($id){
		$rol = Rol::find($id);
		if($rol):
			return response()->json(['rol' => $rol]);
		endif;
		return response()->json(['message' => 'El rol no existe'], 400);
	}

	public function update(UpdateRequest $request, $id){
		$rol = Rol::find($id);
		if($rol):
			try {
				DB::beginTransaction();
				$rol->fill($request->all())->save();
				DB::commit();
				return response()->json(['message' => 'Actualizacion exitosa']);
			}catch (Exception $exception){
				DB::rollBack();
				return response()->json(['message' => 'Ocurrio un error en el proceso', 'exception' => $exception->getMessage()], 400);
			}
		endif;
		return response()->json(['message' => 'El rol no existe'], 400);
	}

	public function delete($id){
		$rol = Rol::withTrashed()->find($id);
		if($rol):
			try {
				DB::beginTransaction();
				if (empty($rol->deleted_at)) :
					$rol->delete();
					DB::commit();
					return response()->json(['message' => 'Desactivación exitosa']);
				endif;
				$rol->restore();
				DB::commit();
				return response()->json(['message' => 'Activación exitosa']);
			}catch (Exception $exception){
				DB::rollBack();
				return response()->json(['message' => 'Ocurrio un error en el proceso', 'exception' => $exception->getMessage()], 400);
			}
		endif;
		return response()->json(['message' => 'El rol no existe'], 400);
	}

}
